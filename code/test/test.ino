/* 
 * Name: dc-motor-driver test.ino
 * 
 * Description: H-bridge DC motor PWM speed and direction control
 * 
 * Author: Krisjanis Rijnieks
 * Last modified: 23 Jun 2021
 * 
 * MIT License
 * 
 * Copyright (c) 2021 Krisjanis Rijnieks
 */

#define MOT_A1 1
#define MOT_A2 2

void setup() {
  // Nothing tp do here.
}

void loop() {
  fwd();
  delay(1000);
  rev();
  delay(1000);
}

void fwd() {
  analogWrite(MOT_A2, LOW);
  
  for (int i = 0; i < 256; i++) {
    analogWrite(MOT_A1, i);
    delay(2);
  }

  delay(2000);

  for (int i = 255; i > -1; i--) {
    analogWrite(MOT_A1, i);
    delay(2);
  }
}

void rev() {
  analogWrite(MOT_A1, LOW);

  for (int i = 0; i < 256; i++) {
    analogWrite(MOT_A2, i);
    delay(1);
  }

  delay(2000);

  for (int i = 255; i > -1; i--) {
    analogWrite(MOT_A2, i);
    delay(1);
  }
}
