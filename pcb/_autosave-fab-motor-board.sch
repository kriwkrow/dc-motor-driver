EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L fab-motor-board-rescue:DC_Power_Jack_CUI_PJ-002AH-SMT-TR-fab-fab-motor-board-rescue J3
U 1 1 607FFEAE
P 1775 1600
F 0 "J3" H 1832 1917 50  0000 C CNN
F 1 "DC_Power_Jack_CUI_PJ-002AH-SMT-TR" H 1832 1826 50  0000 C CNN
F 2 "fab:DC_Power_Jack_CUI_PJ-002AH-SMT-TR" H 1825 1560 50  0001 C CNN
F 3 "https://www.cuidevices.com/product/resource/pj-002ah-smt-tr.pdf" H 1825 1560 50  0001 C CNN
	1    1775 1600
	1    0    0    -1  
$EndComp
$Comp
L fab-motor-board-rescue:A4953_MOTOR_DRIVER-fab-fab-motor-board-rescue U3
U 1 1 608032CB
P 8500 2175
F 0 "U3" H 8500 2642 50  0000 C CNN
F 1 "A4953_MOTOR_DRIVER" H 8500 2551 50  0000 C CNN
F 2 "fab:SOIC-8_3.9x4.9mm_P1.27mm_EP2.41x3.3mm" H 8500 2175 50  0001 C CNN
F 3 "https://www.allegromicro.com/~/media/Files/Datasheets/A4952-3-Datasheet.ashx" H 8500 2175 50  0001 C CNN
	1    8500 2175
	1    0    0    -1  
$EndComp
$Comp
L fab:C C1
U 1 1 60804F79
P 9300 2550
F 0 "C1" H 9415 2596 50  0000 L CNN
F 1 ".1uF" H 9415 2505 50  0000 L CNN
F 2 "fab:C_1206" H 9338 2400 50  0001 C CNN
F 3 "" H 9300 2550 50  0001 C CNN
	1    9300 2550
	1    0    0    -1  
$EndComp
$Comp
L fab:R R2
U 1 1 60808406
P 9350 2100
F 0 "R2" V 9143 2100 50  0000 C CNN
F 1 "0.25" V 9234 2100 50  0000 C CNN
F 2 "fab:R_2010" V 9280 2100 50  0001 C CNN
F 3 "~" H 9350 2100 50  0001 C CNN
	1    9350 2100
	0    1    1    0   
$EndComp
Wire Wire Line
	8900 2100 9200 2100
Wire Wire Line
	9500 2100 9850 2100
Wire Wire Line
	9300 2700 9300 2825
Wire Wire Line
	9850 2700 9850 2825
Wire Wire Line
	8500 2575 8500 2825
Wire Wire Line
	8500 2825 9300 2825
Wire Wire Line
	9300 2825 9850 2825
Connection ~ 9300 2825
Connection ~ 9300 2400
$Comp
L fab-motor-board-rescue:GND-power-fab-motor-board-rescue #PWR020
U 1 1 6080BBB8
P 9850 2100
F 0 "#PWR020" H 9850 1850 50  0001 C CNN
F 1 "GND" V 9855 1972 50  0000 R CNN
F 2 "" H 9850 2100 50  0001 C CNN
F 3 "" H 9850 2100 50  0001 C CNN
	1    9850 2100
	0    -1   -1   0   
$EndComp
$Comp
L fab-motor-board-rescue:GND-power-fab-motor-board-rescue #PWR017
U 1 1 6080C46D
P 8500 3000
F 0 "#PWR017" H 8500 2750 50  0001 C CNN
F 1 "GND" H 8505 2827 50  0000 C CNN
F 2 "" H 8500 3000 50  0001 C CNN
F 3 "" H 8500 3000 50  0001 C CNN
	1    8500 3000
	1    0    0    -1  
$EndComp
Wire Wire Line
	8500 2825 8500 3000
Connection ~ 8500 2825
Wire Wire Line
	8900 2250 9850 2250
Wire Wire Line
	8900 1950 9850 1950
Text Label 9850 2250 2    50   ~ 0
OUT_A1
Text Label 9850 1950 2    50   ~ 0
OUT_A2
Wire Wire Line
	8100 2100 7475 2100
Wire Wire Line
	8100 2250 7475 2250
Text Label 7475 2250 0    50   ~ 0
IN_A1
Text Label 7475 2100 0    50   ~ 0
IN_A2
Wire Wire Line
	8100 1950 7475 1950
$Comp
L fab-motor-board-rescue:GND-power-fab-motor-board-rescue #PWR015
U 1 1 60810399
P 7475 1950
F 0 "#PWR015" H 7475 1700 50  0001 C CNN
F 1 "GND" V 7480 1822 50  0000 R CNN
F 2 "" H 7475 1950 50  0001 C CNN
F 3 "" H 7475 1950 50  0001 C CNN
	1    7475 1950
	0    1    1    0   
$EndComp
Wire Wire Line
	3575 1500 4150 1500
Wire Wire Line
	3275 1800 3275 2225
$Comp
L fab-motor-board-rescue:GND-power-fab-motor-board-rescue #PWR06
U 1 1 60818FBE
P 3275 2225
F 0 "#PWR06" H 3275 1975 50  0001 C CNN
F 1 "GND" H 3280 2052 50  0000 C CNN
F 2 "" H 3275 2225 50  0001 C CNN
F 3 "" H 3275 2225 50  0001 C CNN
	1    3275 2225
	1    0    0    -1  
$EndComp
$Comp
L fab-motor-board-rescue:+5V-power-fab-motor-board-rescue #PWR010
U 1 1 60819DD6
P 4500 1500
F 0 "#PWR010" H 4500 1350 50  0001 C CNN
F 1 "+5V" V 4515 1628 50  0000 L CNN
F 2 "" H 4500 1500 50  0001 C CNN
F 3 "" H 4500 1500 50  0001 C CNN
	1    4500 1500
	0    1    1    0   
$EndComp
Wire Wire Line
	2075 1600 2500 1600
Wire Wire Line
	2500 1600 2500 1700
Wire Wire Line
	2500 1700 2075 1700
Wire Wire Line
	2075 1500 2275 1500
Wire Wire Line
	2500 1700 2500 2000
Connection ~ 2500 1700
$Comp
L fab-motor-board-rescue:GND-power-fab-motor-board-rescue #PWR01
U 1 1 6081C60D
P 2500 2250
F 0 "#PWR01" H 2500 2000 50  0001 C CNN
F 1 "GND" H 2505 2077 50  0000 C CNN
F 2 "" H 2500 2250 50  0001 C CNN
F 3 "" H 2500 2250 50  0001 C CNN
	1    2500 2250
	1    0    0    -1  
$EndComp
$Comp
L fab-motor-board-rescue:PWR_FLAG-power-fab-motor-board-rescue #FLG01
U 1 1 6081D028
P 2275 1500
F 0 "#FLG01" H 2275 1575 50  0001 C CNN
F 1 "PWR_FLAG" H 2275 1673 50  0000 C CNN
F 2 "" H 2275 1500 50  0001 C CNN
F 3 "~" H 2275 1500 50  0001 C CNN
	1    2275 1500
	1    0    0    -1  
$EndComp
$Comp
L fab-motor-board-rescue:PWR_FLAG-power-fab-motor-board-rescue #FLG02
U 1 1 6081D366
P 2500 2000
F 0 "#FLG02" H 2500 2075 50  0001 C CNN
F 1 "PWR_FLAG" V 2500 2128 50  0000 L CNN
F 2 "" H 2500 2000 50  0001 C CNN
F 3 "~" H 2500 2000 50  0001 C CNN
	1    2500 2000
	0    1    1    0   
$EndComp
Connection ~ 2500 2000
Wire Wire Line
	2500 2000 2500 2250
Connection ~ 2275 1500
Wire Wire Line
	2275 1500 2600 1500
$Comp
L fab-motor-board-rescue:+12V-power-fab-motor-board-rescue #PWR02
U 1 1 6081F7B4
P 2600 1150
F 0 "#PWR02" H 2600 1000 50  0001 C CNN
F 1 "+12V" H 2615 1323 50  0000 C CNN
F 2 "" H 2600 1150 50  0001 C CNN
F 3 "" H 2600 1150 50  0001 C CNN
	1    2600 1150
	1    0    0    -1  
$EndComp
Wire Wire Line
	2600 1150 2600 1500
Connection ~ 2600 1500
Wire Wire Line
	2600 1500 2975 1500
Wire Wire Line
	4150 1750 4150 1500
Connection ~ 4150 1500
Wire Wire Line
	4150 1500 4500 1500
$Comp
L fab-motor-board-rescue:GND-power-fab-motor-board-rescue #PWR09
U 1 1 608257E7
P 4150 2950
F 0 "#PWR09" H 4150 2700 50  0001 C CNN
F 1 "GND" H 4155 2777 50  0000 C CNN
F 2 "" H 4150 2950 50  0001 C CNN
F 3 "" H 4150 2950 50  0001 C CNN
	1    4150 2950
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 2525 4150 2950
Wire Wire Line
	4150 2225 4150 2050
$Comp
L fab:LED D1
U 1 1 60822005
P 4150 1900
F 0 "D1" V 4189 1782 50  0000 R CNN
F 1 "LED_PWR" V 4098 1782 50  0000 R CNN
F 2 "fab:LED_1206" H 4150 1900 50  0001 C CNN
F 3 "https://optoelectronics.liteon.com/upload/download/DS-22-98-0002/LTST-C150CKT.pdf" H 4150 1900 50  0001 C CNN
	1    4150 1900
	0    -1   -1   0   
$EndComp
$Comp
L fab-motor-board-rescue:Conn_UPDI_01x02_Male-fab-fab-motor-board-rescue J2
U 1 1 6084944A
P 1750 4425
F 0 "J2" H 1762 4650 50  0000 C CNN
F 1 "Conn_UPDI_01x02_Male" H 1762 4559 50  0000 C CNN
F 2 "fab:Header_SMD_UPDI_01x02_P2.54mm_Horizontal_Male" H 1750 4425 50  0001 C CNN
F 3 "~" H 1750 4425 50  0001 C CNN
	1    1750 4425
	1    0    0    -1  
$EndComp
Wire Wire Line
	6600 4675 7125 4675
Text Label 7125 4675 2    50   ~ 0
UPDI
Wire Wire Line
	1950 4425 2650 4425
Wire Wire Line
	1950 4525 2375 4525
Text Label 2650 4425 2    50   ~ 0
UPDI
$Comp
L fab-motor-board-rescue:GND-power-fab-motor-board-rescue #PWR05
U 1 1 6085D9F6
P 2375 4525
F 0 "#PWR05" H 2375 4275 50  0001 C CNN
F 1 "GND" V 2380 4397 50  0000 R CNN
F 2 "" H 2375 4525 50  0001 C CNN
F 3 "" H 2375 4525 50  0001 C CNN
	1    2375 4525
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6000 5675 6000 5800
$Comp
L fab-motor-board-rescue:+5V-power-fab-motor-board-rescue #PWR011
U 1 1 6087194F
P 6000 4050
F 0 "#PWR011" H 6000 3900 50  0001 C CNN
F 1 "+5V" H 6015 4223 50  0000 C CNN
F 2 "" H 6000 4050 50  0001 C CNN
F 3 "" H 6000 4050 50  0001 C CNN
	1    6000 4050
	1    0    0    -1  
$EndComp
$Comp
L fab-motor-board-rescue:GND-power-fab-motor-board-rescue #PWR012
U 1 1 60872167
P 6000 5800
F 0 "#PWR012" H 6000 5550 50  0001 C CNN
F 1 "GND" H 6005 5627 50  0000 C CNN
F 2 "" H 6000 5800 50  0001 C CNN
F 3 "" H 6000 5800 50  0001 C CNN
	1    6000 5800
	1    0    0    -1  
$EndComp
Text Label 7125 5275 2    50   ~ 0
IN_A1
Text Label 7125 4775 2    50   ~ 0
IN_A2
Wire Wire Line
	6600 5275 7125 5275
Wire Wire Line
	6600 4775 7125 4775
$Comp
L fab-motor-board-rescue:+5V-power-fab-motor-board-rescue #PWR0102
U 1 1 608A1BA3
P 7475 2400
F 0 "#PWR0102" H 7475 2250 50  0001 C CNN
F 1 "+5V" V 7490 2528 50  0000 L CNN
F 2 "" H 7475 2400 50  0001 C CNN
F 3 "" H 7475 2400 50  0001 C CNN
	1    7475 2400
	0    -1   -1   0   
$EndComp
Wire Wire Line
	8100 2400 7475 2400
Wire Wire Line
	8900 2400 9300 2400
$Comp
L fab:Screw_Terminal_01x02_P3.5mm J5
U 1 1 608A9E22
P 9425 4250
F 0 "J5" H 9505 4242 50  0000 L CNN
F 1 "Screw_Terminal_01x02_P3.5mm" H 9505 4151 50  0000 L CNN
F 2 "fab:TerminalBlock_OnShore_1x02_P3.50mm_Horizontal" H 9425 4250 50  0001 C CNN
F 3 "www.on-shore.com/wp-content/uploads/ED555XDS.pdf" H 9425 4250 50  0001 C CNN
	1    9425 4250
	1    0    0    -1  
$EndComp
Wire Wire Line
	9225 4250 8725 4250
Wire Wire Line
	9225 4350 8725 4350
Text Label 8725 4350 0    50   ~ 0
OUT_A2
Text Label 8725 4250 0    50   ~ 0
OUT_A1
$Comp
L fab-motor-board-rescue:+12V-power-fab-motor-board-rescue #PWR021
U 1 1 608C675E
P 10600 2400
F 0 "#PWR021" H 10600 2250 50  0001 C CNN
F 1 "+12V" V 10615 2528 50  0000 L CNN
F 2 "" H 10600 2400 50  0001 C CNN
F 3 "" H 10600 2400 50  0001 C CNN
	1    10600 2400
	0    1    1    0   
$EndComp
$Comp
L fab:Microcontroller_ATtiny412_SSFR U2
U 1 1 608541A1
P 6000 4975
F 0 "U2" H 5470 5021 50  0000 R CNN
F 1 "Microcontroller_ATtiny412_SSFR" H 5470 4930 50  0000 R CNN
F 2 "fab:SOIC-8_3.9x4.9mm_P1.27mm" H 6000 4975 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/40001911A.pdf" H 6000 4975 50  0001 C CNN
	1    6000 4975
	1    0    0    -1  
$EndComp
Wire Wire Line
	6000 4275 6000 4175
Wire Wire Line
	6000 4175 6350 4175
Connection ~ 6000 4175
Wire Wire Line
	6000 4175 6000 4050
$Comp
L fab:C C5
U 1 1 60872AF7
P 6500 4175
F 0 "C5" V 6752 4175 50  0000 C CNN
F 1 "1uF" V 6661 4175 50  0000 C CNN
F 2 "fab:C_1206" H 6538 4025 50  0001 C CNN
F 3 "" H 6500 4175 50  0001 C CNN
	1    6500 4175
	0    -1   -1   0   
$EndComp
Wire Wire Line
	6650 4175 6925 4175
$Comp
L fab-motor-board-rescue:GND-power-fab-motor-board-rescue #PWR03
U 1 1 6087476E
P 6925 4175
F 0 "#PWR03" H 6925 3925 50  0001 C CNN
F 1 "GND" V 6930 4047 50  0000 R CNN
F 2 "" H 6925 4175 50  0001 C CNN
F 3 "" H 6925 4175 50  0001 C CNN
	1    6925 4175
	0    -1   -1   0   
$EndComp
$Comp
L fab:R R1
U 1 1 60823620
P 4150 2375
F 0 "R1" H 4080 2329 50  0000 R CNN
F 1 "1K" H 4080 2420 50  0000 R CNN
F 2 "fab:R_1206" V 4080 2375 50  0001 C CNN
F 3 "~" H 4150 2375 50  0001 C CNN
	1    4150 2375
	-1   0    0    1   
$EndComp
$Comp
L fab-motor-board-rescue:CP-fab-fab-motor-board-rescue C4
U 1 1 6082D71D
P 9850 2550
F 0 "C4" H 9968 2596 50  0000 L CNN
F 1 "100uF" H 9968 2505 50  0000 L CNN
F 2 "fab:CP_Elec_10x10" H 9850 2550 50  0001 C CNN
F 3 "" H 9850 2550 50  0001 C CNN
	1    9850 2550
	1    0    0    -1  
$EndComp
Wire Wire Line
	9300 2400 9850 2400
Connection ~ 9850 2400
Wire Wire Line
	9850 2400 10600 2400
NoConn ~ 6600 5175
NoConn ~ 6600 4875
NoConn ~ 6600 4975
$Comp
L fab:Regulator_Linear_NCP1117-5.0V-1A U1
U 1 1 6086AD49
P 3275 1500
F 0 "U1" H 3275 1742 50  0000 C CNN
F 1 "Regulator_Linear_NCP1117-5.0V-1A" H 3275 1651 50  0000 C CNN
F 2 "fab:SOT-223-3_TabPin2" H 3275 1700 50  0001 C CNN
F 3 "https://www.onsemi.com/pub/Collateral/NCP1117-D.PDF" H 3375 1250 50  0001 C CNN
	1    3275 1500
	1    0    0    -1  
$EndComp
$EndSCHEMATC
