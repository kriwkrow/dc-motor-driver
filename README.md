# Fab DC Motor Board

![DC Motor Driver](images/dc-motor-driver.jpg)

This is a simple DC motor board based on the Allegro MicroSystems A4953ELJTR-T chip. It is designed so that it can be made in a standard fab lab, but features a few parts that one should get on top of the standard fab lab inventory.

There are two versions:

- With polarized capacitor recommended in the A4953 datasheet: [pcb](pcb)
- A board using fab inventory components only: [pcb_fab](pcb_fab)

## Getting Started

Take a look at the test code in the `code` directory. UPDI programming is used to program the board. Consult [megaTinyCore](https://github.com/SpenceKonde/megaTinyCore) documentation for more details.

## BOM

Comming soon...

## Modifications

If you want to make modifications to the KiCad project, make sure you download and install the [Fab KiCad](https://gitlab.fabcloud.org/pub/libraries/electronics/kicad/) library.

## Lincense

[MIT License](LICENSE). Copyright (c) 2021 Krisjanis Rijnieks
